#include "Actor.hpp"
#include "Engine.hpp"

Actor::Actor(int x, int y) :
    m_x {x},
    m_y {y}
{
    engine.map.get_cell(x, y).actor = this;
    engine.map.update_cell(x, y);
}

Actor::~Actor()
{
}

void Actor::moveto(int x, int y)
{
    engine.map.get_cell(m_x, m_y).actor = nullptr;
    engine.map.update_cell(m_x, m_y);

    m_x = x;
    m_y = y;

    engine.map.get_cell(m_x, m_y).actor = this;
    engine.map.update_cell(m_x, m_y);
}
