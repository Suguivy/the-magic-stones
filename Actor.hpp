#ifndef ACTOR_HPP
#define ACTOR_HPP

class Actor {
public:
    Actor(int x, int y);
    ~Actor();
    void moveto(int x, int y);
    inline void move(int dx, int dy) { moveto(m_x + dx, m_y + dy); }
    inline int get_x() { return m_x; }
    inline int get_y() { return m_y; }

private:
    int m_x;
    int m_y;
};

#endif // ACTOR_HPP
