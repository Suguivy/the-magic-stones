#include "Bsp.hpp"
#include "config.hpp"
#include <random>
#include <chrono>

BspNode::BspNode(int x1, int y1, int x2, int y2) :
x1 {x1},
y1 {y1},
x2 {x2},
y2 {y2},
n1 {nullptr},
n2 {nullptr}
{
}

BspNode::~BspNode()
{
    if (n1) delete n1;
    if (n2) delete n2;
}

void BspNode::split(SplitType type, int pos)
{
    if (type == SplitType::HORIZONTAL)
    {
        n1 = new BspNode {x1, y1, x2, pos - 1};
        n2 = new BspNode {x1, pos, x2, y2};
    }
    else if (type == SplitType::VERTICAL)
    {
        n1 = new BspNode {x1, y1, pos - 1, y2};
        n2 = new BspNode {pos, y1, x2, y2};
    }
}

BspTree::BspTree(int x, int y, int width, int height, int divs) :
childs {},
root {x, y, width - 1, height - 1}
{
    recursive_alternated_split(root, divs);
}

void BspTree::recursive_alternated_split(BspNode &node, int split_times)
{
    // TODO: Immprove the use of randomness (don't put random generation variables in the middle of code like the code below)

    // We get the seed with the current time and create the random generator
    auto seed {static_cast<long unsigned>(std::chrono::system_clock::now().time_since_epoch().count())};
    std::minstd_rand0 rand {seed};

    if (split_times > 0)
    {
        // If the remaining splits are odd, we divide horizontally, otherwise vertically
        BspNode::SplitType type {split_times % 2 == 0 ? BspNode::SplitType::HORIZONTAL
            : BspNode::SplitType::VERTICAL};

        // The position on which we divide the node
        int w {node.x2 - node.x1 + 1};
        int h {node.y2 - node.y1 + 1};
        int pos_v {node.x1 + w / 2 + static_cast<int>((static_cast<int>(rand()) % w - 1 - w / 2) * config::bsp_div_factor)};
        int pos_h {node.y1 + h / 2 + static_cast<int>((static_cast<int>(rand()) % h - 1 - h / 2) * config::bsp_div_factor)};
        int pos {split_times % 2 == 0 ? pos_h : pos_v};

        node.split(type, pos);
        recursive_alternated_split(*node.n1, split_times - (rand() % 2 + 1));
        recursive_alternated_split(*node.n2, split_times - (rand() % 2 + 1));
    }
    else
    {
        // The node is a child; we insert it on the childs list
        childs.push_back(node);
    }
}
