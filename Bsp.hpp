#ifndef BSP_HPP
#define BSP_HPP

#include <list>

struct BspNode
{
    enum class SplitType { HORIZONTAL, VERTICAL };
    BspNode(int x1, int y1, int x2, int y2);
    ~BspNode();
    void split(SplitType type, int pos);
    int x1;
    int y1;
    int x2;
    int y2;
    BspNode *n1;
    BspNode *n2;
};

class BspTree {
public:
    BspTree(int x, int y, int width, int height, int divs);
    std::list<BspNode> childs;

private:
    BspNode root;
    void recursive_alternated_split(BspNode &node, int split_times);
};

#endif // BSP_HPP
