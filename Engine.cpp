#include "Engine.hpp"
#include "Terminal.hpp"
#include "Map.hpp"
#include "Actor.hpp"
#include "config.hpp"

Engine::Engine() :
term {},
map {config::map_width, config::map_height},
player {1, 1},
m_exit {false}
{
    map.update();
}

Engine::~Engine()
{
}

void Engine::update()
{
    static char ch;
    static int dx, dy;
    ch = term.inputch();

    switch (ch) {
    case 'h':
        dx = -1, dy = 0;
        break;
    case 'j':
        dx = 0, dy = 1;
        break;
    case 'k':
        dx = 0, dy = -1;
        break;
    case 'l':
        dx = 1, dy = 0;
        break;
    case 'q':
        m_exit = true;
    default:
        break;
    }

    // If hjkl was pressed and cell is empty, the player moves
    if ((dx || dy)
            && map.get_cell(player.get_x() + dx, player.get_y() + dy).is_free())
    {
        player.move(dx, dy);
        dx = 0, dy = 0;
    }
    refresh();
}
