#ifndef ENGINE_HPP
#define ENGINE_HPP

#include "Terminal.hpp"
#include "Actor.hpp"
#include "Map.hpp"

class Engine {
public:
    Engine();
    ~Engine();
    void update();
    inline bool should_exit() { return m_exit; };
    Terminal term;
    Map map;
    Actor player;

private:
    bool m_exit; // Should the game exit?
};

#endif // ENGINE_HPP

extern Engine engine;
