#include "Map.hpp"
#include "Engine.hpp"
#include "Bsp.hpp"
#include "config.hpp"
#include <ncurses.h>
#include <vector>
#include <random>
#include <chrono>

Cell::Cell() :
actor {nullptr},
m_solid {true}
{
}

Map::Map(size_t width, size_t height) :
m_width {width},
m_height {height},
m_grid {width * height, Cell {}}
{

    // We get the seed with the current time and create the random generator
    auto seed {static_cast<long unsigned>(std::chrono::system_clock::now().time_since_epoch().count())};
    std::minstd_rand0 rand {seed};

    // We create the tree dynamically to delete it later
    BspTree *tree = new BspTree {0, 0, static_cast<int>(width), static_cast<int>(height), config::max_bsp_partitions};

    // We create a room for each child (that is not a father)
    for (BspNode &child : tree->childs)
    {
        int min_w {static_cast<int>((child.x2 - child.x1 + 1) * config::min_room_factor)};
        int max_w {static_cast<int>((child.x2 - child.x1 + 1) * config::max_room_factor)};
        int min_h {static_cast<int>((child.y2 - child.y1 + 1) * config::min_room_factor)};
        int max_h {static_cast<int>((child.y2 - child.y1 + 1) * config::max_room_factor)};
        int w {static_cast<int>(rand()) % (max_w - min_w + 1) + min_w};
        int h {static_cast<int>(rand()) % (max_h - min_h + 1) + min_h};

        // Random x and y, that fit on the node area and do not touch the borders of the area
        int x = child.x1 + 1 + rand() % (child.x2 - child.x1 - w);
        int y = child.y1 + 1 + rand() % (child.y2 - child.y1 - h);
        
        dig(x, y, x + w - 1, y + h - 1);
        update();
    }

    delete tree;

    // Update the map cells on the terminal
    update();
}

Map::~Map()
{
}

void Map::update()
{
    // Coordinates to draw the cells
    for (int y {0}; y < static_cast<int>(m_height); y++)
        for (int x {0}; x < static_cast<int>(m_width); x++)
            update_cell(x, y);
}

void Map::update_cell(int x, int y)
{
    Cell &cell {m_grid[x + y * m_width]};

    if (cell.m_solid)
        engine.term.print(x, y, '#');
    else if (cell.actor)
        engine.term.print(x, y, '@');
    else
        engine.term.print(x, y, ' ');
}

void Map::dig(int x1, int y1, int x2, int y2)
{
    for (int x {x1}; x <= x2; x++)
        for (int y {y1}; y <= y2; y++)
            get_cell(x, y).m_solid = false;
}
