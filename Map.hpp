#ifndef MAP_HPP
#define MAP_HPP

#include "Actor.hpp"
#include <vector>

class Cell {
public:
    Cell();
    Actor *actor;
    inline bool is_free() { return !(actor || m_solid); };

private:
    bool m_solid;

friend class Map;
};

class Map {
public:
    Map(size_t width, size_t height);
    ~Map();
    void update();
    void update_cell(int x, int y);
    inline Cell &get_cell(int x, int y) { return m_grid[x + y * m_width]; }
    void dig(int x1, int y1, int x2, int y2);

private:
    size_t m_width;
    size_t m_height;
    std::vector<Cell> m_grid;
};

#endif // MAP_HPP
