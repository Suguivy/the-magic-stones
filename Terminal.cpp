#include "Terminal.hpp"
#include <ncurses.h>
#include <string>

Terminal::Terminal()
{
    initscr();
    noecho();
    curs_set(0);
}

Terminal::~Terminal()
{
    endwin();
}
