#ifndef TERMINAL_HPP
#define TERMINAL_HPP

#include <ncurses.h>
#include <string>

class Terminal {
public:
    Terminal();
    ~Terminal();
    inline char inputch() { return getch(); }
    inline void print(int x, int y, char ch) { mvaddch(y, x, ch); }
};

#endif // TERMINAL_HPP
