#include "config.hpp"

// See config.hpp for indications
namespace config
{
    constexpr size_t map_width {120}, map_height {40};
    constexpr double min_room_factor {0.5}, max_room_factor {0.8};
    constexpr double bsp_div_factor {0.2};
    constexpr int max_bsp_partitions {4};
}
