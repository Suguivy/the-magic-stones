#ifndef CONFIG_HPP
#define CONFIG_HPP

#include <cstddef> // To use size_t type

namespace config
{
    // The width and height of the map
    extern const size_t map_width, map_height;

    // This factors are to get proportional minimum and maximum length of rooms
    // The minimum width of the room is calculed multiplying min_room_factor with the node width,
    // and the maximum width with the max_room_factor. The same applies to the height
    extern const double min_room_factor, max_room_factor;
    
    // This is for determine the variation of the BspNodes separation. A value of 0 will make
    // proportional partitions. It is not remomended a value higher than 1, because is not safe
    extern const double bsp_div_factor;

    // This constant determines the max number of partitions into which the BspTree of the map can be divided.
    extern const int max_bsp_partitions;
}

#endif // CONFIG_HPP
