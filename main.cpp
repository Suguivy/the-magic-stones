#include "Engine.hpp"
#include "Actor.hpp"
#include "Map.hpp"

Engine engine {};

int main()
{
    while (!engine.should_exit())
    {
        engine.update();
    }
    return 0;
}
